package com.example.jens.jbhomeassignment.feed;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jens.jbhomeassignment.R;
import com.example.jens.jbhomeassignment.item.RssItemAdapter;
import com.example.jens.jbhomeassignment.util.ui.RecyclerViewItemClickListener;


/**
 * Fragment containing the RSS feed.
 */
public class RssFeedFragment extends Fragment {
  /**
   * Tag that identifies the {@link RssFeedFragment}.
   */
  public static final String FRAGMENT_TAG = "rss_feed_fragment_tag";

  private RssFeedViewModel rssFeedViewModel;
  private RssItemAdapter rssItemAdapter;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);

    rssFeedViewModel = new RssFeedViewModel();
    rssItemAdapter = new RssItemAdapter(rssFeedViewModel.getRssItemViewModels(), new RecyclerViewItemClickListener() {
      @Override
      public void onItemClick(View v, int position) {
        startBrowser(position);
      }
    });
    rssFeedViewModel.addObserver(rssItemAdapter);
    rssFeedViewModel.loadRssFeedAsync();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_rss_feed, container, false);

    RecyclerView bookshelfRecyclerView = (RecyclerView) view.findViewById(R.id.rss_feed_fragment);
    bookshelfRecyclerView.setHasFixedSize(true);
    bookshelfRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    bookshelfRecyclerView.setAdapter(rssItemAdapter);

    return view;
  }

  /**
   * Start the web browser and go to the link for the RSS item on the specified position in the list.
   */
  private void startBrowser(int position) {
    Uri uri = rssFeedViewModel.getRssItemViewModels().get(position).getLink();
    startActivity(new Intent(Intent.ACTION_VIEW, uri));
  }
}