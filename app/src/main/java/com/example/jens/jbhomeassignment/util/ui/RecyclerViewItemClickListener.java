package com.example.jens.jbhomeassignment.util.ui;

import android.view.View;

/**
 * Item click interface for recycler views.
 */
public interface RecyclerViewItemClickListener {

  /**
   * Called when an item is clicked.
   *
   * @param v        the view that was clicked
   * @param position the position for the item that was clicked
   */
  void onItemClick(View v, int position);
}