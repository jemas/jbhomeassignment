package com.example.jens.jbhomeassignment.item;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jens.jbhomeassignment.R;
import com.example.jens.jbhomeassignment.databinding.RssListItemBinding;
import com.example.jens.jbhomeassignment.feed.RssFeedFragment;
import com.example.jens.jbhomeassignment.util.ui.RecyclerViewItemClickListener;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Adapter for a list of RSS items.
 */
public class RssItemAdapter extends RecyclerView.Adapter<RssItemAdapter.RssItemViewHolder> implements Observer {
  /**
   * Tag for logging.
   */
  private static final String LOG_TAG = RssItemAdapter.class.getSimpleName();

  private final List<RssItemViewModel> rssItemViewModels;
  private final RecyclerViewItemClickListener listener;

  /**
   * Creates a {@link RssItemAdapter}.
   *
   * @param rssItemViewModels the view models for this adapter
   */
  public RssItemAdapter(List<RssItemViewModel> rssItemViewModels, RecyclerViewItemClickListener listener) {
    this.rssItemViewModels = rssItemViewModels;
    this.listener = listener;
  }

  @Override
  public RssItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RssListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout
        .rss_list_item, parent, false);
    return new RssItemViewHolder(binding, listener);
  }

  @Override
  public void onBindViewHolder(RssItemViewHolder holder, int position) {
    RssListItemBinding binding = holder.getBinding();
    binding.setViewModel(rssItemViewModels.get(position));
  }

  @Override
  public int getItemCount() {
    return rssItemViewModels.size();
  }

  @Override
  public void update(Observable o, Object arg) {
    notifyDataSetChanged();
  }

  /**
   * View holder for this adapter.
   */
  static class RssItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final RssListItemBinding binding;
    private final RecyclerViewItemClickListener listener;

    /**
     * Construct a view holder.
     *
     * @param binding  the view binding
     * @param listener item click listener
     */
    RssItemViewHolder(RssListItemBinding binding, RecyclerViewItemClickListener listener) {
      super(binding.getRoot());
      this.binding = binding;
      this.listener = listener;
      binding.getRoot().setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      if (listener != null) {
        listener.onItemClick(v, getLayoutPosition());
      }
    }

    /**
     * Get the the view holders binding for the view it holds.
     */
    private RssListItemBinding getBinding() {
      return binding;
    }


  }
}