package com.example.jens.jbhomeassignment.item;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import org.mcsoxford.rss.MediaThumbnail;
import org.mcsoxford.rss.RSSItem;

import java.util.List;

/**
 * View model representing an RSS item.
 */
// TODO: Show more info in the card view.
public class RssItemViewModel extends BaseObservable {
  private final String title;
  private final String description;
  private final Uri link;

  /**
   * Creates an RSS item view model
   *
   * @param item RSS item containing the data.
   */
  public RssItemViewModel(RSSItem item) {
    this.title = item.getTitle();
    this.description = item.getDescription();
    this.link = item.getLink();
  }

  @Bindable
  public String getTitle() {
    return title;
  }

  @Bindable
  public String getDescription() {
    return description;
  }

  public Uri getLink() {
    // TODO: Maybe move. Note sure this belongs in the view model.
    return link;
  }
}