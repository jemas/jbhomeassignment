package com.example.jens.jbhomeassignment.feed;

import com.example.jens.jbhomeassignment.api.RssAsyncClient;
import com.example.jens.jbhomeassignment.item.RssItemViewModel;

import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * View model for the RSS feed.
 */
class RssFeedViewModel extends Observable implements RssAsyncClient.RssAsyncClientListener {
  /**
   * URI to the RSS feed.
   */
  // TODO: Hardcoded for now. Maybe should be possible for the user to enter the (any?) RSS feed URI.
  private static final String RSS_URI = "http://www.dn.se/nyheter/m/rss/";

  private final List<RssItemViewModel> rssItemViewModels = new ArrayList<>();

  /**
   * Get the list of {@link RssItemViewModel}.
   */
  List<RssItemViewModel> getRssItemViewModels() {
    return rssItemViewModels;
  }

  /**
   * Load the RSS feed async.
   */
  void loadRssFeedAsync() {
    new RssAsyncClient().loadRssFeedAsync(RSS_URI, this);
  }

  @Override
  public void onRssFeedDone(RSSFeed feed) {
    updateRssItemViewModels(feed.getItems());
    notifyViewModelsUpdated();
  }

  @Override
  public void onRssFeedError() {
    // TODO: Implement.
  }

  /**
   * Update the list of RSS item view models.
   */
  private void updateRssItemViewModels(List<RSSItem> items) {
    rssItemViewModels.clear();
    for (RSSItem item : items) {
      rssItemViewModels.add(new RssItemViewModel(item));
    }
  }

  /**
   * Notify all observers that the view models have been updated.
   */
  private void notifyViewModelsUpdated() {
    setChanged();
    notifyObservers();
  }
}