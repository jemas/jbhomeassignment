package com.example.jens.jbhomeassignment.api;

import android.os.AsyncTask;
import android.util.Log;

import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;

/**
 * Reads an RSS feed in an asynchronous manner.
 */
public class RssAsyncClient {
  /**
   * Log tag.
   */
  private static final String TAG = RssAsyncClientListener.class.getSimpleName();

  /**
   * Listener interface for the {@link RssAsyncClient}.
   */
  public interface RssAsyncClientListener {
    /**
     * Callback when an RSS feed has been downloaded.
     *
     * @param feed the RSS feed
     */
    void onRssFeedDone(RSSFeed feed);

    /**
     * Callback when the RSS feed download failed.
     */
    void onRssFeedError();
  }

  /**
   * Loads an RSS feed asynchronously.
   * <p>
   * Note: The callback will be provided on the main thread.
   *
   * @param rssUri   the URI from where to obtain the RSS feed
   * @param listener the listener interested in the result
   */
  public void loadRssFeedAsync(final String rssUri, final RssAsyncClientListener listener) {
    if (listener == null) {
      throw new IllegalArgumentException("A listener must be provided.");
    }

    new AsyncTask<Void, Void, RSSFeed>() {
      @Override
      protected RSSFeed doInBackground(Void... params) {
        RSSFeed feed = null;

        try {
          feed = new RSSReader().load(rssUri);
        } catch (RSSReaderException e) {
          Log.e(TAG, "Failed to load RSS feed", e);
        }

        return feed;
      }

      @Override
      protected void onPostExecute(RSSFeed feed) {
        if (feed == null) {
          listener.onRssFeedError();
        } else {
          listener.onRssFeedDone(feed);
        }
      }
    }.execute();
  }
}